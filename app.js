const express = require ('express');
const mongodb = require ('mongodb');
const bodyParser = require('body-parser');
const assert = require('assert');
// variable needed for object id to be inserted into put function
var objectId = require('mongodb').ObjectID;

//const books = require('/apis/books');

const app = express();
const PORT = 5050;
const DB_URI = 'mongodb+srv://Bryce:23Rooster@cluster0-l3put.mongodb.net/admin?retryWrites=true&w=majority'; 

app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-type");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    next();
})

const DB_NAME = 'book_store';

const books = require('./apis/books')
const Users = require('./apis/Users')
const Book_Genre = require('./apis/Book_Genre')
const Orders = require('./apis/Orders')
const request = require('./apis/request')
const user_history = require('./apis/user_history')

const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true, useNewUrlParser: true});

app.use(bodyParser.urlencoded({extended: false})); // allow user to send data within the URL.
app.use(bodyParser.json()); // allow user to send json data.

app.use('/books',books);
app.use('/user_history',user_history);
app.use('/Users', Users);
app.use('/request',request);
app.use('/Orders', Orders);
app.use('/Book_Genre', Book_Genre);





app.listen(PORT);
console.log("Listening on port " + PORT);