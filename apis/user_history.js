var express = require('express')
var router = express.Router()

const mongodb = require("mongodb");

const DB_NAME = 'book_store';
const USERHISTORY_COLLECTION_NAME = 'user_history';

//const DB_URI = 'mongodb://localhost:27017'; 
const DB_URI = 'mongodb+srv://Bryce:23Rooster@cluster0-l3put.mongodb.net/admin?retryWrites=true&w=majority'
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true, useNewUrlParser: true});

router.get('/', function(req, res){
        client.connect(function(err, connection){
        if(err)
          return res.status(500).send({message:"Connection failed"});
    
            const db = connection.db(DB_NAME); // Connection to the Bookstore DB
            db.collection(USERHISTORY_COLLECTION_NAME)
            .find({})
            .toArray(function(find_err, records){
                      if(find_err)
                        return res.status(500).send({message: "Something went wrong"});
      
                        res.send(records);
                        return res.status(200).send({message: "Record inserted successfully"});
            })
        })
    })
    router.post('/', function(req, res){

            if(!req.body.title || !req.body.author || !req.body.price || !req.body.ISBN || !req.body.rating|| !req.body.stock)
                return res.status(400).send({ message: "title, author, price, ISBN, stock, or rating required."})
        
        
            if(!req.body || req.body.length === 0)
                return res.status(400).send({message: "User_history data is required"})
        
            console.log(req.body); //print an object
            //data is in req.body
        
            client.connect(function(err, connection){
            if(err)
              return res.status(500).send({message:"Connection failed"});
                const db = connection.db(DB_NAME);
                db.collection(USERHISTORY_COLLECTION_NAME)
                .insertOne(req.body, function(insert_error, data){
                    if(insert_error)
                        return res.status(500).send({message: "Something went wrong"});
                    
                    connection.close();
                    return res.status(200).send({message: "Record inserted successfully"});
                })
            });
        })
        router.put('/:id', function (req, res) {

          if(!req.params.id || req.params.id === 0)
                  return res.status(400).send({message: "user_history data is required"})
  
              client.connect(function(err, client){
              if(err)
                return res.status(500).send({message:"Connection failed"});
                  const db = client.db(DB_NAME);
                  db.collection(USERHISTORY_COLLECTION_NAME).updateOne({"_id" : objectId(req.params.id)},                 
                  {$set: req.body},function(err, result) {
                    if(err)
                  return res.status(500).send({message: "Something went wrong"});
          
                      assert.equal(null, err);
                   return res.status(200).send({message: "Updated Successfully"});
              });
            });
          })
          router.delete('/:id', function(req, res){
                var id = req.params.id
            if(!req.params.id || req.params.id === 0)
                return res.status(400).send({message: "User_history data is required"})
                client.connect(function(err, connection){
              if(err)
                return res.status(500).send({message:"Connection failed"});
                    const db = connection.db(DB_NAME);
                    assert.equal(null, err);
                        db.collection(USERHISTORY_COLLECTION_NAME).deleteOne({"_id": objectId(id)}, function(del_err, result){
                          if(del_err)
                            return res.status(500).send({message: "Something went wrong"});
                           assert.equal(null, err);
                           return res.status(200).send({message: "deleted Successfully"});
                })
              })
            })        
            module.exports = router